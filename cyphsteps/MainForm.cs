﻿using cyphsteps.controllers;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace cyphsteps
{
    public partial class MainForm : Form
    {
        private MainController controller;

        public MainForm(MainController _controller)
        {
            InitializeComponent();
            controller = _controller;
        }

        private void tb_entrada_KeyPress(object sender, KeyPressEventArgs e)
        {
            /*if (tb_entrada.Text.Contains("-."))
                tb_salida1.Text += "casa";*/

            if (e.KeyChar == (char)Keys.Return)
            {
                int nProgresses = 4;
                int minProgress = 0;
                int maxProgress = 100;
                int progress = maxProgress / nProgresses;

                cyphsteps.models.cyphers.Cypher c = controller.Cypher;
                c.encode(tb_entrada.Text);
                c.decode(c.GetEncodedBytes(),c.GetO(),c.GetE());

                progressBar1.Value = minProgress;
                tb_salida1.Text = c.GetEncodedText();
                progressBar1.Value += progress;
                tb_salida2.Text = c.GetEncodedBytesString();
                                
                progressBar1.Value += progress;
                tb_salida3.Text = c.GetDecodedText();                
                progressBar1.Value += progress;
                tb_salida4.Text = c.GetDecodedBytesString();
                progressBar1.Value = maxProgress;
            }
        }
    }
}
