﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace cyphsteps.models.cyphers
{
    public class Cypher
    {
        private string deco_text;
        private byte[] bytes;
        private string text;
        private string enco_text;
        private List<byte> enco_bytes;
        private List<byte> deco_bytes;

        private int K;
        private int BM;
        private List<byte> E;
        private List<byte> O;
        
        public Cypher(int key) 
        {
            text = string.Empty;
            enco_text = string.Empty;
            deco_text = string.Empty;
            enco_bytes = new List<byte>();
            BM = (Byte.MaxValue + 1) / 2;
            K = key;
            E = new List<byte>();
            O = new List<byte>();
        }
        
        public void encode(string str)
        {
            text = str;
            //bytes = System.Text.Encoding.UTF8.GetBytes(str);
            bytes = new byte[4]{20,120,240,5};
            enco_text = String.Empty;
            enco_bytes = new List<byte>();
            E = new List<byte>();
            O = new List<byte>();
            int length = bytes.Length;

            for (int i = 0; i < length; i++)
            {
                int B = bytes[i];                
                int M = B * K;                
                int o = M / BM;
                int P = o + K;
                int Q = P * K;
                int c = M - Q;

                O.Add((byte)o);

                if (c <= Byte.MaxValue)
                {
                    enco_bytes.Add((byte)Math.Abs(c));

                    if(c >= 0)
                        E.Add(0);
                    else
                        E.Add(1);
                }
                else
                {
                    int iterations = (int)Math.Floor((double)(c / Byte.MaxValue));
                    int value = c - (Byte.MaxValue * iterations);
                    enco_bytes.Add((byte)(value));

                    if (value >= 0)
                        E.Add(2);
                    else
                        E.Add(3);

                    while (iterations-- != 0)
                    {
                        enco_bytes.Add(Byte.MaxValue);
                        if(iterations != 0)
                            E.Add(2);
                        else
                            E.Add(0);
                    }
                }
            }

            byte[] auxByteArray = enco_bytes.ToArray();
            enco_text = System.Text.Encoding.ASCII.GetString(auxByteArray);
        }

        public void decode(List<byte> bytes, List<byte> o, List<byte> e)
        {
            deco_text = String.Empty;
            deco_bytes = new List<byte>();
            List<int> cInt = new List<int>();
            bool findNext = false;
            int k = 0;

            for(int i = 0 ; i < bytes.Count ; i++)
            {
                if(e[i] == 0)
                {
                    if (!findNext)
                        cInt.Add(bytes[i]);
                    else
                    {
                        if (cInt[k] >= 0)
                            cInt[k] += bytes[i];
                        else
                            cInt[k] -= bytes[i];

                        findNext = false;
                    }

                    k++;
                }

                else if(e[i] == 1)
                    cInt.Add(bytes[i]*-1);

                else if (e[i] == 2 || e[i] == 3)
                {
                    if (!findNext)
                    {
                        if(e[i] == 2)
                            cInt.Add(bytes[i]);

                        else if(e[i] == 3)
                            cInt.Add(bytes[i] * -1);

                        findNext = true;
                    }

                    else
                    {
                        if (cInt[k] >= 0)
                            cInt[k] += bytes[i];
                        else
                            cInt[k] -= bytes[i];
                    }
                }
            }

            for(int i = 0 ; i < cInt.Count ; i++)
            {
                int O = o[i];
                int C = cInt[i];
                int P = O + K;
                int Q = P * K;
                int M = C + Q;
                int D = M / K;
                deco_bytes.Add((byte)D);
            }

            byte[] auxByteArray = deco_bytes.ToArray();
            deco_text = System.Text.Encoding.ASCII.GetString(auxByteArray);
        }

        public byte[] GetBytes()
        {
            return bytes;
        }

        public List<byte> GetDecodedBytes()
        {
            return deco_bytes;
        }

        public List<byte> GetEncodedBytes()
        {
            return enco_bytes;
        }

        public string GetBytesString()
        {
            return bytesToString(bytes);
        }

        public string GetEncodedBytesString()
        {
            return bytesListToString(enco_bytes);
        }

        public string GetDecodedBytesString()
        {
            return bytesListToString(deco_bytes);
        }

        public string GetEncodedText()
        {
            return enco_text;
        }

        public string GetDecodedText()
        {
            return deco_text;
        }

        public string GetText()
        {
            return text;
        }

        public List<byte> GetO()
        {
            return O;
        }

        public List<byte> GetE()
        {
            return E;
        }

        private string bytesListToString(List<byte> bytes)
        {
            string s = "";

            foreach (byte b in bytes)
            {
                s += "[" + Convert.ToString(b) + "] ";
            }

            return s;
        }

        private string bytesToString(byte[] b)
        {
            string s = "";

            for (int i = 0; i < b.Length; i++)
            {
                s += "[" + Convert.ToString(b[i]) + "] ";
            }

            return s;
        }
    }
}
